'use strict';

let secretNumber = Math.trunc(Math.random() * 20) + 1;
let score = 20;
let highScore = 0;

const displayMessage = (message) => {
    document.querySelector('.message').textContent = message;
}

const displayScore = (score) => {
    document.querySelector('.score').textContent = score;
}


document.querySelector('.check').addEventListener('click', function () {
    const guess = Number(document.querySelector('.guess').value);

    // when there is no input
    if (!guess) {
        displayMessage('No Number!');

        // when player wins
    } else if (guess === secretNumber) {
        // document.querySelector('.message').textContent = 'Correct Number!';
        displayMessage('Correct number!')

        document.querySelector('.number').textContent = secretNumber;

        document.querySelector('body').style.backgroundColor = '#60b347';

        document.querySelector('.number').style.width = '30rem';

        if (score > highScore) {
            highScore = score;
            document.querySelector('.highscore').textContent = highScore;
        }

        // when guess is to high or to low
    } else if (guess !== secretNumber) {
        if (score > 1) {
            displayMessage(guess > secretNumber ? 'To high!' : 'To Low!');
            score--;
            displayScore(score);
        } else {
            displayMessage('You lost the game!');
            displayScore(0);
            document.querySelector('body').style.backgroundColor = 'red';
        }
    }
});

// implementing function of again button
document.querySelector('.again').addEventListener('click', function () {
    score = 20;
    secretNumber = Math.trunc(Math.random() * 20) + 1;

    document.querySelector('.number').textContent = '?';


    displayScore(score);


    displayMessage('Start guessing...');

    document.querySelector('.guess').value = '';

    document.querySelector('.number').style.width = '15rem';

    document.querySelector('body').style.backgroundColor = '#222'
})

